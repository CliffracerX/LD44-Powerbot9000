# Powerbot 9001
A proc-gen heavy roguelite-Metroidvania experiment made as a belated Jam entry for Ludum Dare 44.  Source code can be found at: [https://gitlab.com/CliffracerX/LD44-Powerbot9000]

## MANY THANKS TO:
 * LAEMEUR & Zeh Fernando for making the Perfect DOS VGA font series: [http://laemeur.sdf.org/fonts/]
   * It's used as the UI font, because, hey, what better font for a weird robot's operating system then something obviously computery?