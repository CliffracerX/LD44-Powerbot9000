﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Damagable : MonoBehaviour
{
	public float health,maxHealth;
	public GameObject[] drops;
	public float[] dropChances;
	public int[] dropMins,dropMaxes;
	public float dropRange = 0.5f;

	public void Damage(float amount)
	{
		this.health -= amount;
		if(this.health <= 0)
		{
			//print("DED");
			for(int d = 0; d < drops.Length; d++)
			{
				if(Random.Range(0f, 100f) <= dropChances[d])
				{
					int dropA = Random.Range(dropMins[d], dropMaxes[d]);
					for(int i = 0; i<dropA; i++)
					{
						GameObject go = (GameObject)Instantiate(drops[d], transform.position + new Vector3(Random.Range(-dropRange, dropRange), Random.Range(-dropRange, dropRange), 0), transform.rotation);
					}
				}
			}
			Destroy(this.gameObject);
		}
	}
}