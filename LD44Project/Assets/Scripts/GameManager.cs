﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public string tempSeed = "LD44";
	public int mapSize = 10;
	public static GameManager instance;
	public string gameScene;
	public GameObject mainUI,helpUI;
	public InputField textEditSeed;
	public Text sizeDisplay;

	void Start()
	{
		tempSeed = PlayerPrefs.GetString("LastSeed", tempSeed);
		textEditSeed.text = tempSeed;
		mapSize = PlayerPrefs.GetInt("LastSize", mapSize);
		instance = this;
	}

	void Update()
	{
		sizeDisplay.text = "MAP SIZE: " + mapSize;
		tempSeed = textEditSeed.text;
	}

	public void OnUIButtonPressed(int button)
	{
		if(button == 0)
		{
			PlayerPrefs.SetString("LastSeed", tempSeed);
			PlayerPrefs.SetInt("LastSize", mapSize);
			SceneManager.LoadScene(gameScene);
		}
		else if(button == 1) //increment
		{
			mapSize += 1;
		}
		else if(button == 2) //decrement
		{
			mapSize -= 1;
			if(mapSize < 1)
				mapSize = 1;
		}
		else if(button == 3)
		{
			mainUI.SetActive(false);
			helpUI.SetActive(true);
		}
		else if(button == 4)
		{
			mainUI.SetActive(true);
			helpUI.SetActive(false);
		}
		else if(button == -1)
		{
			Application.Quit();
		}
	}
}