﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Bullet : MonoBehaviour
{
	public float damage;
	public float lifeTime;
	public GameObject tempSoundPrefab;
	public bool shouldPlay = false;
	public AudioClip shotClip;
	public float range,rangeMin,vol;
	public Rigidbody2D myRB;
	public Vector2 vel;
	public Color color;
	public SpriteRenderer sprite;
	public float explodeRange = -1;
	public bool lockVel = true;
	/// <summary>
	/// 0 = On creation
	/// 1 = On destroy
	/// </summary>
	public int playTime = 0;

	void Start()
	{
		if(shouldPlay && playTime == 0)
		{
			GameObject go = (GameObject)Instantiate(tempSoundPrefab, transform.position, transform.rotation);
			AudioSource temp = go.GetComponent<TempSound>().mySound;
			temp.clip = shotClip;
			temp.minDistance = rangeMin;
			temp.maxDistance = range;
			temp.volume = vol;
			temp.Play();
		}
	}

	void OnDestroy()
	{
		if(shouldPlay && playTime == 1)
		{
			GameObject go = (GameObject)Instantiate(tempSoundPrefab, transform.position, transform.rotation);
			AudioSource temp = go.GetComponent<TempSound>().mySound;
			temp.clip = shotClip;
			temp.minDistance = rangeMin;
			temp.maxDistance = range;
			temp.volume = vol;
			temp.Play();
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if(other.collider.GetComponent<Damagable>() != null)
		{
			if(other.collider.GetComponent<Damagable>().health > 0)
			{
				other.collider.GetComponent<Damagable>().Damage(damage);
				Destroy(this.gameObject);
			}
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	void Update()
	{
		if(sprite != null)
		{
			sprite.color = color;
		}
		if(lockVel)
		{
			this.myRB.velocity = vel;
		}
		lifeTime -= Time.deltaTime;
		if(lifeTime <= 0)
		{
			if(explodeRange != -1)
			{
				Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, explodeRange);
				foreach(Collider2D c in cols)
				{
					float p = 1 - (Vector3.Distance(transform.position, c.transform.position) / explodeRange);
					print(p);
					if(c.GetComponent<Player>() != null)
					{
						c.GetComponent<Player>().energy -= (int)(((float)damage) * p);
					}
					if(c.GetComponent<Damagable>() != null)
					{
						c.GetComponent<Damagable>().Damage((int)(((float)damage) * p));
					}
				}
			}
			Destroy(this.gameObject);
		}
	}
}