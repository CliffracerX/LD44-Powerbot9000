﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	public int maxSpawned = 3;
	public List<GameObject> spawned;
	public GameObject[] options;
	public float[] probabilities;
	public float minSpawnDist,maxSpawnDist;

	void Update()
	{
		float dist = Vector3.Distance(transform.position, Player.instance.transform.position);
		if(dist < minSpawnDist || dist > maxSpawnDist)
		{
			return; //don't operate if the player is far off or too close
		}
		List<GameObject> tempList = new List<GameObject>();
		for(int i = 0; i < spawned.Count; i++)
		{
			if(spawned[i] == null)
			{
				tempList.Add(spawned[i]);
			}
		}
		foreach(GameObject i in tempList)
		{
			spawned.Remove(i);
		}
		int ticks = 0;
		while(spawned.Count < maxSpawned && ticks < 100)
		{
			ticks++;
			bool found = false;
			int toSpawnID = 0;
			while(!found && ticks < 100)
			{
				ticks++;
				toSpawnID = Random.Range(0, options.Length);
				if(Random.Range(0f, 100f) <= probabilities[toSpawnID])
					found = true;
			}
			spawned.Add((GameObject)Instantiate(options[toSpawnID], transform.position, transform.rotation));
		}
	}
}