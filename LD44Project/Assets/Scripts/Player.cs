﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Weapon
{
	public Color bodyColor, crystalColor;
	public int wepType,bodyID,crystalID;
	public float energyPerShot,damagePerShot,energyChargeRate,energyPerCharge,energy,maxEnergy;
	public float fireSpeed;
	public string name, powerReq, DPS;
	public bool fullAuto;
	public GameObject bulletObj;
	public float spread,speed;
	public int numShots;

	public Weapon(int type, int bodyID, int crystalID, Color bodyColor, Color crystalColor)
	{
		this.wepType = type;
		this.bodyID = bodyID;
		this.crystalID = crystalID;
		this.bodyColor = bodyColor;
		this.crystalColor = crystalColor;
	}
}

[System.Serializable]
public class SpriteList
{
	public Sprite[] list;
}

[System.Serializable]
public class WeaponMod
{
	public float energyDraw;
	public float speedIncrease,fireIncrease;
	public float energyIncreaseMin,energyIncreaseMax;
	public bool autoAdd;
	public float damageIncreaseMin,damageIncreaseMax;
	public float rechargeRate,rechargeGiven;
	public GameObject bulletObj;
	public float spread;
	public int multishotAdd;
	public string name;
}

public class Player : MonoBehaviour
{
	public int guiScale = -1;
	public float speed,jumpForce;
	public float energy,maxEnergy,fireCooldown;
	public Texture2D barStart,barStartColor,barFill,barFillBG,barCap,barStartWep,barStartMelee;
	public Color playerCol = Color.yellow;
	public Sprite[] playerBody,playerBodyColor,playerBodyGlow,playerArms;
	public Sprite[] playerArmsPrimary,playerArmsSpecial,playerArmsHeavy;
	public AudioSource[] music;
	public AudioClip[] track1,track2,track3,track4;
	public int trackOld,trackCurrent;
	public bool[] playing;
	public float[] volume;
	public Transform arms;
	public int curFrame;
	public Weapon ranged1,ranged2,melee;
	public int curWep;
	public SpriteRenderer bodyMain,bodyColor,bodyGlow,bodyArms,bodyArmsPoint,weaponBody,weaponCrystal;
	public bool wepOut;
	public Animator myAnim;
	public Rigidbody2D myBody;
	public SpriteRenderer cursor;
	public Texture2D[] chars;
	public SpriteList[] primaryBody,primaryCrystal,specialBody,specialCrystal,heavyBody,heavyCrystal;
	public Color[] playerColOptions, wepBodyColOptions, wepCrystalColOptions;
	public WeaponMod[] primaryBodyMods, primaryCrystalMods, specialBodyMods, specialCrystalMods, heavyBodyMods, heavyCrystalMods, traits;
	public Transform shootPos;
	public static Player instance;
	public bool hasSolarUpgrade;
	public float miasmaTime = 0;
	public float oldEnergy,adrenaline;

	void Start()
	{
		ranged1 = GenerateWeapon(Random.Range(0, 2));
		ranged2 = GenerateWeapon(Random.Range(1, 3));
		this.playerCol = GenerateColor(0.0f, 1.0f, 0.75f, 1.0f, 0.75f, 1.0f);
		instance = this;
	}

	void OnTriggerStay2D(Collider2D other)
	{
		if(other.name == "Miasma")
		{
			energy -= Time.deltaTime * 25f;
			miasmaTime = 5;
		}
		if(other.name == "Light")
		{
			energy += Time.deltaTime * (hasSolarUpgrade ? 15f : 1f);
			energy = Mathf.Clamp(energy, 0, maxEnergy);
			if(miasmaTime > 4.5f)
			{
				energy += Time.deltaTime * 15f;
			}
		}
	}

	public Weapon GenerateWeapon(int type)
	{
		WeaponMod bodyT = null, crystalT = null;
		int body = 0;
		int crystal = 0;
		if(type == 0)
		{
			body = Random.Range(0, primaryBodyMods.Length);
			crystal = Random.Range(0, primaryCrystalMods.Length);
			bodyT = primaryBodyMods[body];
			crystalT = primaryCrystalMods[crystal];
		}
		else if(type == 1)
		{
			body = Random.Range(0, specialBodyMods.Length);
			crystal = Random.Range(0, specialCrystalMods.Length);
			bodyT = specialBodyMods[body];
			crystalT = specialCrystalMods[crystal];
		}
		else if(type == 2)
		{
			body = Random.Range(0, heavyBodyMods.Length);
			crystal = Random.Range(0, heavyCrystalMods.Length);
			bodyT = heavyBodyMods[body];
			crystalT = heavyCrystalMods[crystal];
		}
		Weapon w = new Weapon(type, body, crystal, GenerateColor(0.0f, 1.0f, 0.25f, 0.75f, 0.125f, 0.75f), GenerateColor(0.0f, 1.0f, 0.75f, 1.0f, 0.75f, 1.0f));
		w.energyPerShot += bodyT.energyDraw;
		w.energyPerShot += crystalT.energyDraw;
		w.damagePerShot += Random.Range((int)bodyT.damageIncreaseMin, (int)bodyT.damageIncreaseMax);
		w.damagePerShot += Random.Range((int)crystalT.damageIncreaseMin, (int)crystalT.damageIncreaseMax);
		w.maxEnergy += (Random.Range((int)bodyT.energyIncreaseMin, (int)bodyT.energyIncreaseMax)*25);
		w.maxEnergy += (Random.Range((int)crystalT.energyIncreaseMin, (int)crystalT.energyIncreaseMax)*25);
		w.fireSpeed += bodyT.fireIncrease;
		w.fireSpeed += crystalT.fireIncrease;
		w.fullAuto = bodyT.autoAdd || crystalT.autoAdd;
		w.energyChargeRate += bodyT.rechargeRate;
		w.energyChargeRate += crystalT.rechargeRate;
		w.energyPerCharge += bodyT.rechargeGiven;
		w.energyPerCharge += crystalT.rechargeGiven;
		w.bulletObj = bodyT.bulletObj;
		w.spread += bodyT.spread;
		w.spread += crystalT.spread;
		w.numShots += bodyT.multishotAdd;
		w.numShots += crystalT.multishotAdd;
		w.speed += bodyT.speedIncrease;
		w.speed += crystalT.speedIncrease;
		if(Random.Range(0f, 100f) <= 25f) //25% chance of being given a Trait
		{
			WeaponMod trait = traits[Random.Range(0, traits.Length)];
			w.energyPerShot *= trait.energyDraw;
			w.maxEnergy *= Random.Range(trait.energyIncreaseMin, trait.energyIncreaseMax);
			w.maxEnergy = Mathf.CeilToInt(w.maxEnergy / 25) * 25;
			w.damagePerShot *= Random.Range(trait.damageIncreaseMin, trait.damageIncreaseMax);
			w.damagePerShot = Mathf.Ceil(w.damagePerShot);
			w.fireSpeed *= trait.fireIncrease;
			w.spread *= trait.spread;
			w.energyChargeRate *= trait.rechargeRate;
			w.energyPerCharge *= trait.rechargeGiven;
			w.name += trait.name;
		}
		w.name += crystalT.name;
		w.name += bodyT.name;
		w.energy = w.maxEnergy;
		if(w.bulletObj == null)
		{
			w.bulletObj = crystalT.bulletObj;
		}
		return w;
	}

	public Color GenerateColor(float hueMin, float hueMax, float satMin, float satMax, float valMin, float valMax)
	{
		/*Color colA = list[Random.Range(0, list.Length)];
		Color colB = list[Random.Range(0, list.Length)];
		return Color.Lerp(colA, colB, Random.Range(0.0f, 1.0f));*/
		return Color.HSVToRGB(Random.Range(hueMin, hueMax), Random.Range(satMin, satMax), Random.Range(valMin, valMax));
	}

	public Color Collerp(Color a, Color b, float amountR, float amountG, float amountB)
	{
		return new Color(Mathf.Lerp(a.r, b.r, amountR), Mathf.Lerp(a.g, b.g, amountG), Mathf.Lerp(a.b, b.b, amountB));
	}

	void OnGUI()
	{
		int lGuiScale = guiScale;
		if(lGuiScale == -1)
		{
			lGuiScale = (int)Mathf.Ceil((Screen.height / 1.0f) / 960f)*2;
		}
		int px = 5;
		GUI.DrawTexture(ScalePos(px, 5, barStart.width, barStart.height, lGuiScale), barStart);
		GUI.color = playerCol;
		GUI.DrawTexture(ScalePos(px, 5, barStart.width, barStart.height, lGuiScale), barStartColor);
		GUI.color = Color.white;
		px += barStart.width;
		int meTemp = (int)(maxEnergy / 4f);
		GUI.DrawTexture(ScalePos(px, 5, meTemp, barFillBG.height, lGuiScale), barFillBG);
		GUI.color = playerCol;
		GUI.DrawTexture(ScalePos(px, 5, (int)(meTemp*(energy/maxEnergy)), barFillBG.height, lGuiScale), barFill);
		GUI.color = Color.white;
		px += meTemp;
		GUI.DrawTexture(ScalePos(px, 5, barCap.width, barCap.height, lGuiScale), barCap);
		px -= (meTemp / 2);
		char[] temp = (Mathf.RoundToInt(energy)+"/"+Mathf.FloorToInt(maxEnergy)).ToCharArray();
		int[] tempI = GenIntTemp(temp);
		px -= (chars[0].width * tempI.Length)/2;
		for(int i = 0; i < tempI.Length; i += 1)
		{
			GUI.DrawTexture(ScalePos(px, 6, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
			px += chars[0].width;
		}
		if(wepOut)
		{
			Weapon t = null;
			if(curWep == 2)
			{
				t = melee;
			}
			else if(curWep == 1)
			{
				t = ranged2;
			}
			else
			{
				t = ranged1;
			}
			px = 5;
			GUI.DrawTexture(ScalePos(px, 13, barStart.width, barStart.height, lGuiScale), barStart);
			GUI.color = t.crystalColor;
			GUI.DrawTexture(ScalePos(px, 13, barStart.width, barStart.height, lGuiScale), curWep == 2 ? barStartMelee : barStartWep);
			GUI.color = Color.white;
			px += barStart.width;
			meTemp = (int)(t.maxEnergy / 4f);
			GUI.DrawTexture(ScalePos(px, 13, meTemp, barFillBG.height, lGuiScale), barFillBG);
			GUI.color = t.crystalColor;
			GUI.DrawTexture(ScalePos(px, 13, (int)(meTemp*(t.energy/t.maxEnergy)), barFillBG.height, lGuiScale), barFill);
			GUI.color = Color.white;
			px += meTemp;
			GUI.DrawTexture(ScalePos(px, 13, barCap.width, barCap.height, lGuiScale), barCap);
			px -= (meTemp / 2);
			temp = (Mathf.RoundToInt(t.energy)+"/"+Mathf.FloorToInt(t.maxEnergy)).ToCharArray();
			tempI = GenIntTemp(temp);
			px -= (chars[0].width * tempI.Length)/2;
			for(int i = 0; i < tempI.Length; i += 1)
			{
				GUI.DrawTexture(ScalePos(px, 14, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
				px += chars[0].width;
			}
			px = barStart.width + meTemp + 6 + barCap.width;
			temp = (t.name.ToUpper()+", "+Mathf.CeilToInt(t.damagePerShot)+" DP/S").ToCharArray();
			tempI = GenIntTemp(temp);
			for(int i = 0; i < tempI.Length; i += 1)
			{
				GUI.DrawTexture(ScalePos(px, 14, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
				px += chars[0].width;
			}
		}
		Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.5f);
		foreach(Collider2D c in cols)
		{
			Vector2 tempPos = Camera.main.WorldToScreenPoint(c.transform.position);
			//print(tempPos);
			px = (int)tempPos.x;
			int py = (int)tempPos.y;
			px /= lGuiScale;
			py /= lGuiScale;
			if(c.name == "BatteryPedestal(Clone)")
			{
				temp = "Install Battery Pack".ToUpper().ToCharArray();
				tempI = GenIntTemp(temp);
				for(int i = 0; i < tempI.Length; i += 1)
				{
					GUI.DrawTexture(ScalePos(px, py, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
					px += chars[0].width;
				}
			}
			else if(c.name == "Battery(Clone)")
			{
				temp = "Absorb Energy Pod".ToUpper().ToCharArray();
				tempI = GenIntTemp(temp);
				for(int i = 0; i < tempI.Length; i += 1)
				{
					GUI.DrawTexture(ScalePos(px, py, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
					px += chars[0].width;
				}
			}
			else if(c.name == "SpringUpgrade")
			{
				temp = "Install Spring Boots".ToUpper().ToCharArray();
				tempI = GenIntTemp(temp);
				for(int i = 0; i < tempI.Length; i += 1)
				{
					GUI.DrawTexture(ScalePos(px, py, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
					px += chars[0].width;
				}
			}
			else if(c.name == "SolarUpgrade")
			{
				temp = "Install Solar Panel".ToUpper().ToCharArray();
				tempI = GenIntTemp(temp);
				for(int i = 0; i < tempI.Length; i += 1)
				{
					GUI.DrawTexture(ScalePos(px, py, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
					px += chars[0].width;
				}
			}
			else if(c.name == "Escape")
			{
				temp = "Win".ToUpper().ToCharArray();
				tempI = GenIntTemp(temp);
				for(int i = 0; i < tempI.Length; i += 1)
				{
					GUI.DrawTexture(ScalePos(px, py, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
					px += chars[0].width;
				}
			}
			else if(c.GetComponent<WeaponPedestal>() != null)
			{
				WeaponPedestal p = c.GetComponent<WeaponPedestal>();
				Weapon t = p.wep;
				GUI.DrawTexture(ScalePos(px, py, barStart.width, barStart.height, lGuiScale), barStart);
				GUI.color = t.crystalColor;
				GUI.DrawTexture(ScalePos(px, py, barStart.width, barStart.height, lGuiScale), curWep == 2 ? barStartMelee : barStartWep);
				GUI.color = Color.white;
				px += barStart.width;
				meTemp = (int)(t.maxEnergy / 4f);
				GUI.DrawTexture(ScalePos(px, py, meTemp, barFillBG.height, lGuiScale), barFillBG);
				GUI.color = t.crystalColor;
				GUI.DrawTexture(ScalePos(px, py, (int)(meTemp*(t.energy/t.maxEnergy)), barFillBG.height, lGuiScale), barFill);
				GUI.color = Color.white;
				px += meTemp;
				GUI.DrawTexture(ScalePos(px, py, barCap.width, barCap.height, lGuiScale), barCap);
				int pxT = px;
				px -= (meTemp / 2);
				temp = (Mathf.RoundToInt(t.energy)+"/"+Mathf.FloorToInt(t.maxEnergy)).ToCharArray();
				tempI = GenIntTemp(temp);
				px -= (chars[0].width * tempI.Length)/2;
				for(int i = 0; i < tempI.Length; i += 1)
				{
					GUI.DrawTexture(ScalePos(px, py+1, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
					px += chars[0].width;
				}
				px = pxT + 1 + barCap.width;
				temp = (t.name.ToUpper()+", "+Mathf.CeilToInt(t.damagePerShot)+" DP/S").ToCharArray();
				tempI = GenIntTemp(temp);
				for(int i = 0; i < tempI.Length; i += 1)
				{
					GUI.DrawTexture(ScalePos(px, py+1, chars[tempI[i]].width, chars[tempI[i]].height, lGuiScale), chars[tempI[i]]);
					px += chars[0].width;
				}
			}
		}
	}

	Rect ScalePos(int x, int y, int w, int h, int amount) //code re-used from the original build of The Fallen (and its later revamp) for easily scaling a Rect
	{
		return new Rect(x*amount, y*amount, w*amount, h*amount);
	}

	int[] GenIntTemp(char[] temp) //code re-used from The Fallen's Revamp for drawing a fancy pixely number without hassling with fonts and dictionaries.
	{
		int[] tempI = new int[temp.Length];
		for(int i = 0; i < temp.Length; i++)
		{
			if(temp[i] == '0')
			{
				tempI[i] = 0;
			}
			else if(temp[i] == '1')
			{
				tempI[i] = 1;
			}
			else if(temp[i] == '2')
			{
				tempI[i] = 2;
			}
			else if(temp[i] == '3')
			{
				tempI[i] = 3;
			}
			else if(temp[i] == '4')
			{
				tempI[i] = 4;
			}
			else if(temp[i] == '5')
			{
				tempI[i] = 5;
			}
			else if(temp[i] == '6')
			{
				tempI[i] = 6;
			}
			else if(temp[i] == '7')
			{
				tempI[i] = 7;
			}
			else if(temp[i] == '8')
			{
				tempI[i] = 8;
			}
			else if(temp[i] == '9')
			{
				tempI[i] = 9;
			}
			else if(temp[i] == 'x')
			{
				tempI[i] = 10;
			}
			else if(temp[i] == '/')
			{
				tempI[i] = 11;
			}
			else if(temp[i] == 'A')
			{
				tempI[i] = 12;
			}
			else if(temp[i] == 'B')
			{
				tempI[i] = 13;
			}
			else if(temp[i] == 'C')
			{
				tempI[i] = 14;
			}
			else if(temp[i] == 'D')
			{
				tempI[i] = 15;
			}
			else if(temp[i] == 'E')
			{
				tempI[i] = 16;
			}
			else if(temp[i] == 'F')
			{
				tempI[i] = 17;
			}
			else if(temp[i] == 'G')
			{
				tempI[i] = 18;
			}
			else if(temp[i] == 'H')
			{
				tempI[i] = 19;
			}
			else if(temp[i] == 'I')
			{
				tempI[i] = 20;
			}
			else if(temp[i] == 'J')
			{
				tempI[i] = 21;
			}
			else if(temp[i] == 'K')
			{
				tempI[i] = 22;
			}
			else if(temp[i] == 'L')
			{
				tempI[i] = 23;
			}
			else if(temp[i] == 'M')
			{
				tempI[i] = 24;
			}
			else if(temp[i] == 'N')
			{
				tempI[i] = 25;
			}
			else if(temp[i] == 'O')
			{
				tempI[i] = 26;
			}
			else if(temp[i] == 'P')
			{
				tempI[i] = 27;
			}
			else if(temp[i] == 'Q')
			{
				tempI[i] = 28;
			}
			else if(temp[i] == 'R')
			{
				tempI[i] = 29;
			}
			else if(temp[i] == 'S')
			{
				tempI[i] = 30;
			}
			else if(temp[i] == 'T')
			{
				tempI[i] = 31;
			}
			else if(temp[i] == 'U')
			{
				tempI[i] = 32;
			}
			else if(temp[i] == 'V')
			{
				tempI[i] = 33;
			}
			else if(temp[i] == 'W')
			{
				tempI[i] = 34;
			}
			else if(temp[i] == 'X')
			{
				tempI[i] = 35;
			}
			else if(temp[i] == 'Y')
			{
				tempI[i] = 36;
			}
			else if(temp[i] == 'Z')
			{
				tempI[i] = 37;
			}
			else if(temp[i] == ' ')
			{
				tempI[i] = 38;
			}
			else if(temp[i] == ',')
			{
				tempI[i] = 39;
			}
			//\u221E
		}
		return tempI;
	}

	void LateUpdate()
	{
		float move = Input.GetAxis("Horizontal") * speed * Time.deltaTime * (wepOut ? 1f : 2f);
		myAnim.SetBool("Walk", move != 0);
		if(move != 0)
		{
			if(!wepOut)
			{
				transform.localScale = new Vector3(move <= 0 ? 1 : -1, 1, 1);
			}
			transform.position += new Vector3(move, 0, 0);
		}
	}

	void Update()
	{
		if(energy <= 0)
		{
			SceneManager.LoadScene("Scenes/LoseScene");
		}
		miasmaTime -= Time.deltaTime;
		Weapon t = null;
		if(curWep == 2)
		{
			t = melee;
		}
		else if(curWep == 1)
		{
			t = ranged2;
		}
		else
		{
			t = ranged1;
		}
		bodyMain.sprite = playerBody[curFrame];
		bodyColor.sprite = playerBodyColor[curFrame];
		bodyColor.color = playerCol;
		bodyGlow.sprite = playerBodyGlow[curFrame];
		bodyGlow.color = playerCol;
		if(!wepOut)
		{
			bodyArms.sprite = playerArms[curFrame];
			bodyArms.gameObject.SetActive(true);
			bodyArmsPoint.gameObject.SetActive(false);
		}
		else
		{
			if(t.wepType == 2)
			{
				bodyArmsPoint.sprite = playerArmsHeavy[curFrame];
				weaponBody.sprite = heavyBody[t.bodyID].list[curFrame];
				weaponCrystal.sprite = heavyCrystal[t.crystalID].list[curFrame];
			}
			else if(t.wepType == 1)
			{
				bodyArmsPoint.sprite = playerArmsSpecial[curFrame];
				weaponBody.sprite = specialBody[t.bodyID].list[curFrame];
				weaponCrystal.sprite = specialCrystal[t.crystalID].list[curFrame];
			}
			else
			{
				bodyArmsPoint.sprite = playerArmsPrimary[curFrame];
				weaponBody.sprite = primaryBody[t.bodyID].list[curFrame];
				weaponCrystal.sprite = primaryCrystal[t.crystalID].list[curFrame];
			}
			weaponBody.color = t.bodyColor;
			weaponCrystal.color = t.crystalColor;
			bodyArms.gameObject.SetActive(false);
			bodyArmsPoint.gameObject.SetActive(true);
		}
		bool grounded = false;
		RaycastHit2D hit = Physics2D.Raycast(transform.position + (Vector3.down * 0.55f), Vector2.down, 0.25f);
		if(hit != null)
		{
			if(Vector2.Distance(transform.position, hit.point) < 0.6f)
			{
				grounded = true;
			}
		}
		if(Input.GetButtonDown("Jump") && grounded)
		{
			myBody.AddForce(new Vector2(0, jumpForce));
		}
		if(Input.GetButtonDown("SwapWeapons"))
		{
			if(curWep == 0)
				curWep = 1;
			else
				curWep = 0;
		}
		myAnim.SetFloat("AnimSpeed", (wepOut ? 1f : 2f));
		if(Input.GetButtonDown("ToggleWeapons"))
		{
			wepOut = !wepOut;
		}
		if(Input.GetButtonDown("Interact"))
		{
			Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, 0.5f);
			foreach(Collider2D c in cols)
			{
				if(c.name == "BatteryPedestal(Clone)")
				{
					energy = maxEnergy = maxEnergy + 25;
					Destroy(c.gameObject);
				}
				else if(c.name == "Battery(Clone)")
				{
					energy += 25;
					if(energy > maxEnergy)
						energy = maxEnergy;
					Destroy(c.gameObject);
				}
				else if(c.name == "SpringUpgrade")
				{
					jumpForce *= 1.25f;
					Destroy(c.gameObject);
				}
				else if(c.name == "SolarUpgrade")
				{
					hasSolarUpgrade = true;
					Destroy(c.gameObject);
				}
				else if(c.name == "Escape")
				{
					SceneManager.LoadScene("Scenes/WinScene");
				}
				else if(c.GetComponent<WeaponPedestal>() != null)
				{
					WeaponPedestal p = c.GetComponent<WeaponPedestal>();
					Weapon temp = ranged1;
					if(curWep == 1)
					{
						temp = ranged2;
						ranged2 = p.wep;
						p.wep = temp;
					}
					else
					{
						ranged1 = p.wep;
						p.wep = temp;
					}
					p.Refresh();
				}
			}
		}
		if(Input.GetButton("Recharge"))
		{
			if(energy > t.energyChargeRate * Time.deltaTime && t.energy < t.maxEnergy)
			{
				energy -= t.energyChargeRate * Time.deltaTime;
				t.energy += t.energyPerCharge * Time.deltaTime;
				if(t.energy > t.maxEnergy)
				{
					float temp = t.energy - t.maxEnergy;
					energy += (temp / t.energyPerCharge) * t.energyChargeRate;
				}
			}
		}
		else
		{
			if(Input.GetButton("Fire1") && wepOut)
			{
				if(t.energy >= t.energyPerShot && fireCooldown <= 0)
				{
					fireCooldown = t.fireSpeed;
					t.energy -= t.energyPerShot;
					Camera.main.transform.Translate(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0).normalized * (t.damagePerShot / 100f));
					for(int i = 0; i < t.numShots; i++)
					{
						GameObject bullet = (GameObject)Instantiate(t.bulletObj, shootPos.position, shootPos.rotation);
						Vector2 vel = (-bullet.transform.right + new Vector3(Random.Range(-t.spread, t.spread), Random.Range(-t.spread, t.spread), 0)) * t.speed * transform.localScale.x;
						if(bullet.GetComponent<Rigidbody2D>() != null)
						{
							bullet.GetComponent<Rigidbody2D>().AddForce(vel);
						}
						if(bullet.GetComponent<Bullet>() != null)
						{
							if(i == 0)
							{
								bullet.GetComponent<Bullet>().shouldPlay = true;
							}
							bullet.GetComponent<Bullet>().vel = vel;
							bullet.GetComponent<Bullet>().damage = t.damagePerShot;
							bullet.GetComponent<Bullet>().color = t.crystalColor;
							bullet.GetComponent<Bullet>().sprite.color = t.crystalColor;
						}
					}
				}
				if(t.fullAuto)
					fireCooldown -= Time.deltaTime;
			}
			else
			{
				fireCooldown -= Time.deltaTime;
			}
		}
		Collider2D[] colsM = Physics2D.OverlapCircleAll(transform.position, 8f);
		foreach(Collider2D c in colsM)
		{
			if(c.name == "Miasma")
			{
				miasmaTime = 1f;
			}
		}
		if(energy < oldEnergy)
		{
			adrenaline += (oldEnergy - energy) / (energy / maxEnergy);
		}
		adrenaline -= (adrenaline * 0.1f * Time.deltaTime * (energy / maxEnergy));
		playing[0] = energy >= (maxEnergy / 5);
		playing[1] = wepOut;
		playing[2] = miasmaTime >= 0;
		playing[3] = adrenaline >= (maxEnergy / 10f);
		for(int i = 0; i < 4; i++)
		{
			if(playing[i])
				volume[i] = Mathf.Clamp(volume[i] += Time.deltaTime, 0, 1); //TODO: add the ability to select music volume on an options menu?
			else
				volume[i] = Mathf.Clamp(volume[i] -= Time.deltaTime, 0, 1);
			music[i].volume = volume[i];
			if(trackCurrent != trackOld)
			{
				if(i == 3)
				{
					trackOld = trackCurrent;
				}
				AudioClip[] temp = track1;
				if(i == 1)
					temp = track2;
				else if(i == 2)
					temp = track3;
				else if(i == 3)
					temp = track4;
				music[i].clip = temp[trackCurrent];
			}
		}
		Vector3 tempV3 = Vector3.Lerp(Camera.main.transform.position, transform.position, 8f * Time.deltaTime);
		Camera.main.transform.position = new Vector3(tempV3.x, tempV3.y, Camera.main.transform.position.z);
		cursor.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		cursor.transform.position = new Vector3(cursor.transform.position.x, cursor.transform.position.y, 0);
		if(wepOut)
		{
			if(cursor.transform.position.x != transform.position.x)
			{
				transform.localScale = new Vector3(cursor.transform.position.x <= transform.position.x ? 1 : -1, 1, 1);
			}
		}
		bodyArmsPoint.transform.LookAt(cursor.transform.position, bodyArmsPoint.transform.up);
		bodyArmsPoint.transform.Rotate(0, 90, 0);
		Quaternion q = bodyArmsPoint.transform.rotation;
		q.eulerAngles = new Vector3(0, 0, q.eulerAngles.z);
		bodyArmsPoint.transform.rotation = q;
		oldEnergy = energy;
	}
}