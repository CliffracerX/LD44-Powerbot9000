﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
	public string destinationSceneName;

	void Update()
	{
		if(Input.anyKey)
		{
			SceneManager.LoadScene(destinationSceneName);
		}
	}
}