﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : MonoBehaviour
{
	public Damagable myDamage;
	public Rigidbody2D myBody;
	public float speed, jumpPow, attackPower, attackSpeed, attackCooldown, despawnDist;

	void LateUpdate()
	{
		if(Vector3.Distance(Player.instance.transform.position, transform.position) > despawnDist)
		{
			Destroy(this.gameObject);
		}
		else
		{
			float speed = this.speed * Time.deltaTime * (Player.instance.transform.position.x < transform.position.x ? -1f : 1f);
			transform.position += new Vector3(speed, 0, 0);
			if(Player.instance.transform.position.y > transform.position.y)
			{
				RaycastHit2D hit = Physics2D.Raycast(transform.position + (Vector3.down * 0.6f), Vector2.down);
				if(hit != null)
				{
					if(Vector2.Distance(transform.position, hit.point) < 0.1f)
					{
						myBody.AddForce(new Vector2(0, jumpPow));
					}
				}
			}
			transform.localScale = new Vector3((Player.instance.transform.position.x > this.transform.position.x ? -1f : 1f), 1f, 1f);
		}
		attackCooldown -= Time.deltaTime;
	}

	void OnCollisionStay2D(Collision2D other)
	{
		if(other.collider.GetComponent<Player>() != null && attackCooldown <= 0)
		{
			attackCooldown = attackSpeed;
			other.collider.GetComponent<Player>().energy -= attackPower;
		}
	}
}