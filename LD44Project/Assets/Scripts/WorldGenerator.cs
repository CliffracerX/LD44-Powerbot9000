﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WorldTile
{
	public List<Vector2> usedTileSpots;
	public List<Vector2> exits;
	public List<ExitDir> exitDirs;
	public enum ExitDir {Up=0, Left=1, Down=2, Right=3}
	public ExitDir myDir;
	public GameObject obj;
}

[System.Serializable]
public class WorldTier
{
	public bool generatedSpecial;
	public WorldTile[] normalTiles;
	public WorldTile[] specialTiles;
	public WorldTile[] endTiles;
}

public class WorldGenerator : MonoBehaviour
{
	public WorldTier[] tiers;
	public int curTier;
	public List<Vector2> usedSpaces,listExits,listExitsNext;
	public List<Vector2> exitsUp,exitsLeft,exitsDown,exitsRight;
	public List<WorldTile.ExitDir> listExitDirs,listExitDirsNext;
	public int worldSizeTemp,maxWorldSize;
	public float tileRadius;
	int ticks;
	public int seed;
	public static bool done = false;
	public GameObject player;

	void Start()
	{
		seed = GameManager.instance.tempSeed.GetHashCode();
		maxWorldSize = GameManager.instance.mapSize;
		Random.InitState(seed);
	}

	void Update()
	{
		if(listExits.Count > 0 || listExitDirsNext.Count > 0)
		{
			ticks += 1;
			if(ticks > 100000000 || curTier >= tiers.Length)
			{
				return;
			}
			if(listExits.Count < 1)
			{
				for(int i = 0; i < listExitsNext.Count; i++)
				{
					listExits.Add(listExitsNext[i]);
					listExitDirs.Add(listExitDirsNext[i]);
				}
				listExitsNext.Clear();
				listExitDirsNext.Clear();
			}
			//int exitID = Random.Range(0, listExits.Count);
			for(int i = 0; i < listExits.Count; i++)
			{
				if(ContainsKey(usedSpaces, listExits[i]))
				{
					listExits.RemoveAt(i);
					listExitDirs.RemoveAt(i);
				}
			}
			int exitID = 0;
			if(!ContainsKey(usedSpaces, listExits[exitID]))
			{
				WorldTier rand = tiers[curTier];
				if(rand.generatedSpecial)
				{
					if(listExits.Count > 2 || curTier == tiers.Length - 1)
					{
						WorldTile tile = PickTileFor(rand.endTiles, exitID);
						SpawnTile(tile, listExits[exitID]);
						listExits.RemoveAt(exitID);
						listExitDirs.RemoveAt(exitID);
					}
					else
					{
						curTier += 1;
						worldSizeTemp = 0;
					}
				}
				else
				{
					if(worldSizeTemp >= maxWorldSize && !rand.generatedSpecial)
					{
						WorldTile tile = PickTileFor(rand.specialTiles, exitID);
						SpawnTile(tile, listExits[exitID]);
						listExits.RemoveAt(exitID);
						listExitDirs.RemoveAt(exitID);
						rand.generatedSpecial = true;
					}
					else
					{
						WorldTile tile = PickTileFor(rand.normalTiles, exitID);
						if(tile != null)
						{
							SpawnTile(tile, listExits[exitID]);
						}
						else
						{
							tile = PickTileFor(rand.endTiles, exitID);
							SpawnTile(tile, listExits[exitID]);
						}
						listExits.RemoveAt(exitID);
						listExitDirs.RemoveAt(exitID);
					}
				}
			}
			else
			{
				listExits.RemoveAt(exitID);
				listExitDirs.RemoveAt(exitID);
			}
		}
		else
		{
			player.SetActive(true);
			this.enabled = false;
		}
	}

	WorldTile PickTileFor(WorldTile[] listIn, int exitID)
	{
		Vector2 exit = listExits[exitID];
		WorldTile.ExitDir dir = listExitDirs[exitID];
		bool foundTile = false;
		WorldTile tile = null;
		int lTicks = 0;
		List<WorldTile> list = new List<WorldTile>();
		foreach(WorldTile wt in listIn)
		{
			if(wt.myDir == dir)
				list.Add(wt);
		}
		if(list.Count == 0)
		{
			print("WARNING WARNING WARNING: NO VALID TILES WERE FOUND FOR INPUT DIRECTION: " + dir);
			return null;
		}
		while(!foundTile && lTicks <= 999)
		{
			lTicks += 1;
			int randID = Random.Range(0, list.Count);
			tile = list[randID];
			foundTile = true;
			if(tile.myDir != dir)
			{
				print("DIR FAIL: Requested "+dir+" at "+exit+", but tile #"+randID+" had direction "+tile.myDir+"!");
				foundTile = false;
			}
			foreach(Vector2 space in tile.usedTileSpots)
			{
				if(ContainsKey(usedSpaces, space+exit))
				{
					print("SPACE FAIL: Tile already present in space at requested spawn for exit " + exit);
					foundTile = false;
				}
			}
			//TODO: try and stop mundane blocked exits from forming?
			/*foreach(Vector2 space in tile.exits)
			{
				if(ContainsKey(usedSpaces, space + exit))
				{
					if(tile.myDir == WorldTile.ExitDir.Up)
					{
						if(!ContainsKey(exitsUp, space + exit))
						{
							print("Connection failure w/ direction UP when generating tile for " + exit);
							foundTile = false;
						}
					}
					else if(tile.myDir == WorldTile.ExitDir.Left)
					{
						if(!ContainsKey(exitsLeft, space + exit))
						{
							print("Connection failure w/ direction LEFT when generating tile for " + exit);
							foundTile = false;
						}
					}
					else if(tile.myDir == WorldTile.ExitDir.Down)
					{
						if(!ContainsKey(exitsDown, space + exit))
						{
							print("Connection failure w/ direction DOWN when generating tile for " + exit);
							foundTile = false;
						}
					}
					else if(tile.myDir == WorldTile.ExitDir.Right)
					{
						if(!ContainsKey(exitsRight, space + exit))
						{
							print("Connection failure w/ direction RIGHT when generating tile for " + exit);
							foundTile = false;
						}
					}
				}
			}*/
		}
		if(!foundTile)
		{
			tile = null;
			print("WARNING: TILE SELECTION FAILURE AFTER 999 ATTEMPTS?");
		}
		return tile;
	}

	void SpawnTile(WorldTile tile, Vector2 exit)
	{
		if(tile == null)
			return;
		Instantiate(tile.obj, (new Vector3(exit.x, exit.y, 0)*tileRadius) + new Vector3(0, 0, 0.001f), Quaternion.identity);
		foreach(Vector2 vec in tile.usedTileSpots)
		{
			usedSpaces.Add(vec+exit);
			worldSizeTemp += 1;
		}
		for(int i = 0; i < tile.exits.Count; i++)
		{
			Vector2 exitNew = tile.exits[i];
			listExitsNext.Add(exitNew + exit);
			WorldTile.ExitDir dir = tile.exitDirs[i];
			listExitDirsNext.Add(dir);
			if(dir == WorldTile.ExitDir.Up)
				exitsUp.Add(exitNew + exit);
			else if(dir == WorldTile.ExitDir.Left)
				exitsLeft.Add(exitNew + exit);
			if(dir == WorldTile.ExitDir.Down)
				exitsDown.Add(exitNew + exit);
			else if(dir == WorldTile.ExitDir.Right)
				exitsRight.Add(exitNew + exit);
		}
	}

	bool ContainsKey(List<Vector2> list, Vector2 key)
	{
		foreach(Vector2 v in list)
		{
			if(v.x == key.x && v.y == key.y)
				return true;
		}
		return false;
	}
}