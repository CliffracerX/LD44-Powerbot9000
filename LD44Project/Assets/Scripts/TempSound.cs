﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempSound : MonoBehaviour
{
	public AudioSource mySound;

	void Update()
	{
		if(!mySound.isPlaying)
		{
			Destroy(this.gameObject);
		}
	}
}