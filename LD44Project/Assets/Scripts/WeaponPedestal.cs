﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPedestal : MonoBehaviour
{
	public Weapon wep;
	public SpriteRenderer body,crystal;
	public bool gen;

	void Update()
	{
		if(!gen && Player.instance != null)
		{
			wep = Player.instance.GenerateWeapon(Random.Range(0, 3));
			this.Refresh();
			gen = true;
			this.enabled = false;
		}
	}

	public void Refresh()
	{
		SpriteList bodyList = null;
		SpriteList crystalList = null;
		if(wep.wepType == 2)
		{
			bodyList = Player.instance.heavyBody[wep.bodyID];
			crystalList = Player.instance.heavyCrystal[wep.crystalID];
		}
		else if(wep.wepType == 1)
		{
			bodyList = Player.instance.specialBody[wep.bodyID];
			crystalList = Player.instance.specialCrystal[wep.crystalID];
		}
		else
		{
			bodyList = Player.instance.primaryBody[wep.bodyID];
			crystalList = Player.instance.primaryCrystal[wep.crystalID];
		}
		body.sprite = bodyList.list[0];
		crystal.sprite = crystalList.list[0];
		body.color = wep.bodyColor;
		crystal.color = wep.crystalColor;
	}
}