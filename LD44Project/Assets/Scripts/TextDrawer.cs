﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDrawer : MonoBehaviour
{
	public float drawTime,drawSpeed;
	[TextArea(25, 999)]
	public string goalString;
	public string tempString;
	public Text myText;
	public bool cursorTemp = true;
	public SceneChanger sc;

	void Update()
	{
		drawTime -= Time.deltaTime;
		if(drawTime <= 0 && tempString.Length < goalString.Length)
		{
			tempString += goalString[tempString.Length];
			drawTime = drawSpeed;
		}
		else if(drawTime <= 0)
		{
			cursorTemp = !cursorTemp;
			drawTime = 0.5f;
			sc.enabled = true;
		}
		myText.text = tempString + (cursorTemp ? "_" : " ");
	}
}