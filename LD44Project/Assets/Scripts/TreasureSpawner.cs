﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureSpawner : MonoBehaviour
{
	public GameObject[] options;
	public float[] probabilities;

	void Start()
	{
		GameObject spawned = null;
		int ticks = 0;
		while(ticks < 200)
		{
			ticks += 1;
			int rand = Random.Range(0, options.Length);
			if(Random.Range(0f, 100f) < probabilities[rand])
			{
				spawned = options[rand];
				ticks = 1500;
				break;
			}
		}
		if(spawned != null)
		{
			Instantiate(spawned, transform.position, transform.rotation);
		}
	}
}